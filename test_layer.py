import random
import numpy as np
from datetime import datetime
# from sklearn import preprocessing
import tensorflow as tf
import keras
from keras.utils import plot_model
from keras.layers import Input, Embedding, LSTM, Dense, Dropout, GRU, BatchNormalization, Layer, Lambda, Add, Flatten
from keras.models import Model, load_model
from keras.layers import LeakyReLU, PReLU, ELU, ThresholdedReLU, ReLU 
from keras.regularizers import l1, l2
from keras import backend as K
from keras.activations import relu, selu
import pickle as pk
from layers.dense import MyDense

########################
# load data
x_train = np.load('gen_dump_data/train_x.npy')
y_train = np.load('gen_dump_data/train_y.npy')
print(x_train.shape)
print(y_train.shape)

x_val = np.load('gen_dump_data/val_x.npy')
y_val = np.load('gen_dump_data/val_y.npy')

########################

pair_input = Input(shape=(50,), name='pair_input')

# x = Flatten()(pair_input)
x = MyDense(10, kernel_regularizer=l2(2e-1), name='layer1')(pair_input)
x = ELU()(x)
x = MyDense(10)(x)
x = ELU()(x)

output = MyDense(1)(x)

model = Model(inputs=pair_input, outputs=output)
learning_rate = 1e-4

sgd = keras.optimizers.SGD(lr=learning_rate, momentum=0.9, decay=3e-6, nesterov=True)
rmsprop = keras.optimizers.RMSprop(lr=learning_rate, rho=0.9, epsilon=None, decay=0.0)
adam = keras.optimizers.Adam(lr=learning_rate, beta_1=0.9, beta_2=0.999, epsilon=None, decay=1e-6, amsgrad=False)
nadam = keras.optimizers.Nadam(lr=learning_rate, beta_1=0.9, beta_2=0.999, epsilon=None, schedule_decay=0.004)
adamax = keras.optimizers.Adamax(lr=learning_rate, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.001)
model.compile(optimizer=nadam, loss='mse',
    metrics=['mse'],
    )

model.summary()

history = model.fit(x_train, y_train,
	batch_size=16,
	epochs=10,
	verbose=1,
	validation_data=(x_val, y_val)
	)
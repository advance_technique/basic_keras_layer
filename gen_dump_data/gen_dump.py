import math
import numpy as np 
import matplotlib.pyplot as plt

sin_wave = np.array([np.sin(x) for x in np.arange(20000)])

# plt.plot(sin_wave[:100])
# plt.show()

X = []
Y = []

seq_len = 50
num_records = len(sin_wave) - seq_len

for i in range(num_records - 500):
    X.append(sin_wave[i:i+seq_len])
    Y.append(sin_wave[i+seq_len])

X = np.array(X)
Y = np.array(Y)


# print(X.shape, Y.shape)

X_val = []
Y_val = []

for i in range(num_records - 500, num_records):
    X_val.append(sin_wave[i:i+seq_len])
    Y_val.append(sin_wave[i+seq_len])

X_val = np.array(X_val)
Y_val = np.array(Y_val)

np.save('train_x.npy' ,X)
np.save('train_y.npy' ,Y)
np.save('val_x.npy' ,X_val)
np.save('val_y.npy' ,Y_val)
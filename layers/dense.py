from keras import backend as K 
from keras.layers import Layer 
import tensorflow as tf 
import numpy as np

class MyDense(Layer):
    """simple Dense layer customs by keras"""
    def __init__(self, output_dim,
                kernel_regularizer=None, 
                **kwargs):
        super(MyDense, self).__init__(**kwargs)
        self.output_dim = output_dim
        self.kernel_regularizer=kernel_regularizer

    def build(self, input_shape):
        # create weight
        self.weight = self.add_weight(name='weight',
                                      shape=(input_shape[1], self.output_dim),
                                      initializer='uniform',
                                      regularizer=self.kernel_regularizer,
                                      trainable=True)
        self.bias = self.add_weight(name='bias',
                                shape=(self.output_dim,),
                                initializer='zeros',
                                trainable=True)
        # b_init = tf.zeros_initializer()
        # self.bias = tf.Variable(initial_value=b_init(shape=(self.output_dim,),
        #                                           dtype='float32'),
        #                      trainable=True)


        super(MyDense, self).build(input_shape)
        
    def call(self, x):
        return K.dot(x, self.weight) + self.bias
        # return K.dot(x, self.weight)

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.output_dim)

# x = tf.ones((2,2))
# dense = MyDense(3)
# y = dense(x)
# print((x))
# print(y)
# tf.print(x)
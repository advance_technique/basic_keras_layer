from keras import backend as K 
from keras.layers import Layer 
import tensorflow as tf 
import numpy as np
from keras.layers import Input, RNN, Dense
from keras.models import Model, load_model

# not finish yet
class MyRnn:
    """repeatly run RNN cell to get out state"""
    # def __init__(self, **kwargs):
    #     super(MyRnn, self).__init__(**kwargs)
    def SimRnn(self, units, inputs_data, return_sequence=False, kernel_regularizer=None):
        shape_input = inputs_data.shape
        outputs = []
        states = []
        temp_state = tf.zeros(units)
        print(temp_state)
        for i in range(shape_input[1]):
            temp_output, temp_state = MyRnn_cell(units)([tf.gather(inputs_data, i), temp_state])
            # outputs.append(temp_output)
            # states.append(temp_state)
            print(i)
        # if return_sequence==False:
        #     return outputs[-1]
        # else:
        #     return tf.convert_to_tensor(outputs)

class MyRnn_cell(Layer):
    """simple RNN cell customs by keras"""
    def __init__(self, units,
                kernel_regularizer=None, 
                **kwargs):
        super(MyRnn_cell, self).__init__(**kwargs)
        self.units = units
        self.state_size = units
        self.kernel_regularizer=kernel_regularizer

    def build(self, input_shape):
        assert isinstance(input_shape, list)

        # create weight
        self.weight = self.add_weight(name='weight',
                      shape=(input_shape[0][-1], self.units),
                      initializer='uniform',
                      regularizer=self.kernel_regularizer,
                      trainable=True)
        self.recurrent_weight = self.add_weight(name='recurrent_weight',
                                shape=(self.units, self.units),
                                initializer='uniform',
                                regularizer=self.kernel_regularizer,
                                trainable=True)
        self.weight_state = self.add_weight(name='weight_state',
                      shape=(self.units, self.units),
                      initializer='uniform',
                      regularizer=self.kernel_regularizer,
                      trainable=True)

        self.bias_state = self.add_weight(name='bias_state',
                    shape=(self.units,),
                    initializer='zeros',
                    trainable=True)

        self.bias_out = self.add_weight(name='bias_out',
                    shape=(self.units,),
                    initializer='zeros',
                    trainable=True)

        self.build = True

    def call(self, inputs):
        if not isinstance(inputs, list):
            raise ValueError('This layer should be called on a list of inputs.')
        slide_input = inputs[0]
        hidden_state = inputs[1]
        h = K.dot(slide_input, self.weight)
        new_state = h + K.dot(hidden_state, self.recurrent_weight) + self.bias_state
        new_state = tf.math.tanh(new_state)

        output = self.bias_out + K.dot(new_state, self.weight_state)
        output = tf.math.softmax(output)
        return output, new_state

    def compute_output_shape(self, x):
        assert isinstance(input_shape, list)
        shape_a, shape_b = input_shape
        return [(shape_a[0], self.units)]


x = tf.ones((100, 4, 5))
input_cell = Input((4, 5))
# print(input_cell.shape)
a = MyRnn()
a.SimRnn(10, input_cell)
# x = tf.ones((2,2))
# dense = MyRnn_cell(3)
# y = dense(x)
# print((x))
# print(y)
# tf.print(x)

# cell = MyRnn_cell(10)
# input_cell = Input((None, 5))
# layer = RNN(cell)
# y = layer(input_cell)
# # y = layer(y)
# output = Dense(1)(y)
# model = Model(inputs=input_cell, outputs=output)
# print(y)

# model.summary()